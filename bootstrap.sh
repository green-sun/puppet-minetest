#!/bin/bash

# Update package repository before installing
apt-get update

# Install dependencies for running puppet/cloning code.
apt-get install -y git puppet
