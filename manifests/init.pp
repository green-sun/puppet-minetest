class minetest (
        $dirroot = '/opt/minetest.git',
        $defaultpass = 'chickensandwich',
        $serverdescription = "Welcome to Minetest on $::hostname",
        $mirrorbaselocation = 'https://github.com/minetest',
        $branch = 'stable-0.4',
    ) {
    
    Package { ensure => "latest" }

    case $::operatingsystem {
        'centos', 'redhat', 'fedora', 'scientific': {

        }
        'ubuntu', 'debian': {
            $minetest_server_packages = [ "build-essential", "cmake", "libbz2-dev", "libcurl4-gnutls-dev", "libfreetype6-dev", "libgettextpo-dev", "libgl1-mesa-dev", "libgmp-dev", "libhiredis-dev", "libirrlicht-dev", "libjpeg-dev", "libjsoncpp-dev", "libleveldb-dev", "libluajit-5.1-dev", "libncurses-dev", "libogg-dev", "libopenal-dev", "libpng12-dev", "libspatialindex-dev", "libsqlite3-dev", "libvorbis-dev", "libxxf86vm-dev", "libx11-dev", "libx32ncursesw5-dev", "patch", "redis-server", "zlib1g-dev" ]

            $extra_commands_minetest_server = ""
        }
        'freebsd': {

        }
        'darwin': {

        }
        'default': {
            $extra_commands_minetest_server = ""
        }
    }

    package { $minetest_server_packages: install_options => ['--force-yes'] }
    ->
    user { "minetest":
        ensure => present,
        system => true,
    }
    ->
    group { "minetest": ensure => present, }
    ->
    exec { "clone_minetest":
        command => "bash -c 'if [ ! -d $dirroot ]; then git clone --depth 1 -b $branch $mirrorbaselocation/minetest.git $dirroot; else cd $dirroot && git pull; fi'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    exec { "clone_minetest_game":
        command => "bash -c 'if [ ! -d $dirroot/games/minetest_game ]; then git clone --depth 1 $mirrorbaselocation/minetest_game.git $dirroot/games/minetest_game; else cd $dirroot/games/minetest_game && git pull; fi'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    file { "$dirroot":
        ensure  => directory,
        mode    => 750,
        owner   => "minetest",
        group   => "minetest",
        source => "puppet:///modules/minetest/minetest",
        recurse => true,
    }
    ->
    exec { "prebuild_minetest":
        command => "bash -c 'cd $dirroot && cmake -DRUN_IN_PLACE=TRUE -DBUILD_SERVER=TRUE -ENABLE_REDIS=TRUE -ENABLE_LEVELDB=TRUE -ENABLE_LUAJIT=TRUE -ENABLE_CURSES=TRUE .'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    exec { "build_minetest":
        command => "bash -c 'cd $dirroot && make -j 1'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    file { "$dirroot/minetest.conf":
        ensure  => file,
        mode    => 644,
        owner   => "minetest",
        group   => "minetest",
        content => template('minetest/minetest.conf.erb'),
    }
    ->
    exec { "delete_worlds_from_redis":
        command => "bash -c 'ls $dirroot/worlds/ | xargs -l1 -I {} redis-cli del {}'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    exec { "convert_worlds_to_redis":
        command => "bash -c 'ls $dirroot/worlds/ | xargs -l1 -I {} $dirroot/bin/minetestserver --migrate redis --world \"$dirroot/worlds/{}\"'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    notify{"To start MineTest server with specified game run: su minetest -c '$dirroot/bin/minetestserver --config $dirroot/minetest.conf'": }

    if $extra_commands_minetest_server != '' {
        exec { "extra_commands_minetest_server":
            command => "$extra_commands_minetest_server",
            path    => "/usr/local/bin/:/bin/:/usr/sbin/",
        }

    }
}
