# GreenSun Minetest puppet module
This is the GreenSun puppet module for installing/managing [Minetest server](https://github.com/minetest/minetest) along with the default [minetest_game](https://github.com/minetest/minetest_game) required for most content to run, as well as steps to automatically switch all included worlds over to the [Redis](https://redis.io/) in-memory database for faster performance.  It also includes the following additions, with a huge shout-out and thank you to all content creators - the whole purpose of this puppet module is to show off their amazing work and speed it up a notch :)
* A collection of freely available [worlds](https://forum.minetest.net/viewtopic.php?f=12&t=9440)
* Several [mobs](http://wiki.minetest.net/Mods:Mobs) so you have some company
* Extra [subgames](http://wiki.minetest.net/Subgames) so you can go on adventures

The only requirement is to have [Vagrant](https://www.vagrantup.com/) installed.  The included Vagrantfile will work to do the following on running `vagrant up --provision`:
* Provision a VM based off the community public release of [Ubuntu Server 16.04](https://www.ubuntu.com/download/server), which can be updated anytime from within your Vagrant root directory on your host machine by running `vagrant box update`
* Install a Redis database according to default configuration from upstream repositories, noting the authors are no experts in server tuning - improvements to the default configuration are welcomed.
* Compile Minetest server based on the latest master branch from the specified $mirrorbaselocation (or the default upstream GitHub repo if no other value is provided) according to the [manifests/default.pp](manifests/default.pp) file.  The server should be up and available from your host machine's Minetest client at 192.168.222.43:30000 for any user with the default password 'chickensandwich' once the server is started with `su minetest -c '/opt/minetest.git/bin/minetestserver --config /opt/minetest.git/minetest.conf`

I tried to keep things setup completely based out of the root directoy /opt/minetest to keep things simple.  If you want to switch to a different world just edit the name in /opt/minetest/minetest.conf to be one of the worlds listed in the /opt/minetest/worlds/ directory, then run the startup command.  Also note that all users have all the privileges I could enable by default, and when you login the first time and/or switch maps you may drop in-game and find yourself falling downward off the map - DON'T PANIC!!  You have fly and run privs enabled by default so you just need to immediately press K and J, respectively, to enable them.  Then you can hold the jump (space) button to fly back up to "reality" and get your bearings.

Generous thanks to all the following authors I was able to grep emails of out of the install directory, and to any who did not have a standard-format email address I may have unintentionally omitted - please feel free to issue a pull request adding yourself to the README and I'll be happy to include if you've contributed work and I've missed crediting you, and sincere apologies if so:

`/greensun-minetest.git$ grep -Iohr '<[[:graph:]]\+@[[:graph:]]\+[.][[:graph:]]\+>' ./files/minetest/ | sort | uniq`
* <blockmen2015@gmail.com>
* <celeron55@gmail.com>
* <cornernote@gmail.com>
* <dazeg9@gmail.com>
* <frederico@teia.bio.br>
* <kaeza@users.sf.net>
* <kahrl@gmx.net>
* <lkaezadl3@gmail.com>
* <lkaezadl3@yahoo.com>
* <maxk@qualcomm.com>
* <mitroman@naturalnet.de>
* <mk@realbadangel.pl>
* <sam@hocevar.net>
* <sapier@achernar.lan>
* <sapier@does.not.have.an.email>
* <tigran@aivazian.fsnet.co.uk>
* <vanessaezekowitz@gmail.com>
